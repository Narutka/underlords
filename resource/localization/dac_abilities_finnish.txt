"lang"
{ 
	"Language" "finnish" 
	"Tokens" 
	{
		// ANTIMAGE
		"dac_ability_antimage_mana_break"										"Mana Break"
		"dac_ability_antimage_mana_break_Description"							"PASSIIVINEN: Anti-Mage polttaa {s:mana_per_hit} vihollisen manaa jokaisella iskulla. Mana Break tekee tekee 50 % viedystä manasta vahinkona kohteeseen."
		"dac_ability_antimage_mana_break_Lore"									"Anti-Mage on tullut noutamaan minut. Minä olen valmis. – <i>Turoth, Tyler Estate -karkuri</i>"

		//QUEEN OF PAIN
		"dac_ability_queenofpain_scream_of_pain"								"Scream Of Pain"
		"dac_ability_queenofpain_scream_of_pain_Description"					"Queen of Pain päästää kimakan huudon ja tuhoaa kaikki viholliset {s:radius} ruudun säteellä ja aiheuttaa {s:damage} vahinkoa."
		"dac_ability_queenofpain_scream_of_pain_Lore"							"Akasha ei ole ehkä läheinen ystävä muttaa auttaa mieluusti, jos hyötyy siitä myöhemmin. – <i>Anessix, käytännönläheinen demoni</i>"

		//LINA
		"dac_ability_lina_laguna_blade"											"Laguna Blade"
		"dac_ability_lina_laguna_blade_Description"								"Lina iskee salamalla ja jakaa {s:damage} vahinkoa yhdelle yksikölle."
		"dac_ability_lina_laguna_blade_Lore"									"Lina oli helppo saada tulemaan Valkoiseen torniin. Arvostamme toistemme töitä. – <i>Hobgen, tuhopolton ammattilainen</i>"

		//FURION
		"dac_ability_natures_prophet_summon_tree"								"Nature's Call"
		"dac_ability_natures_prophet_summon_tree_Description"					"Nature's Prophet kutsuu treantin laudan reunalta. Treantti on druidi."
		"dac_ability_natures_prophet_summon_tree_Lore"							"Enno on ottanut yhteyttä metsän eläimiin. Se voi tietää ongelmia. – <i>O'nia, Revtelin karkotettu kauppias</i>"

		//WINDRUNNER
		"dac_ability_windrunner_powershot"										"Powershot"
		"dac_ability_windrunner_powershot_Description"							"Windranger jännittää joustaan enintään {s:castPoint} sekunnin ajan ja ampuu yhden tarkan nuolen ja aiheuttaa {s:powershot_damage} vahinkoa. Nuolen tekemä vahinko vähenee {s:damage_reduction} % jokaisesta osuman saaneesta vihollisesta."
		"dac_ability_windrunner_powershot_Lore"									"Sain matkoillani monta uutta ystävää enkä pelkää pyytää palveluksia. – <i>Wren, maailmanmatkaaja</i>"

		//WITCHDOCTOR
		"dac_ability_witch_doctor_paralyzing_cask"								"Paralyzing Cask"
		"dac_ability_witch_doctor_paralyzing_cask_Description"					"Witchdoctor viskaa tyrmäävää jauhetta sisältävän tynnyrin, joka ponnahtaa {s:bounces} kertaa vihollisten joukossa lamauttaen ja jakaen {s:damage} vahinkoa niille, joihin osuu."
		"dac_ability_witch_doctor_paralyzing_cask_Lore"							"On ilo seurata, kun teet töitä, Doc... – <i>Hobgen, kiusantekijäkeiju</i>"

		//JUGGERNAUT
		"dac_ability_juggernaut_blade_fury"										"Blade Fury"
		"dac_ability_juggernaut_blade_fury_Description"							"Juggernaut kieppuu tikarin kanssa, mikä tekee siitä taioille immuunin. Se jakaa {s:damage} tuhoa sekunnissa vihollisille {s:radius} ruudun säteellä {s:duration} sekunnin ajan."
		"dac_ability_juggernaut_blade_fury_Lore"								"Yurnero ei ole mikä tahansa soturi, vaan hän on Juggernaut. Jos saamme hänet omalle puolellemme, ruumiita alkaa tulla. – <i>Grek  katsoo asiaa kaikista näkökulmista.</i>"

		//SANDKING
		"dac_ability_sandking_burrowstrike"										"Burrowstrike"
		"dac_ability_sandking_burrowstrike_Description"							"Sand King kaivautuu maahan ja syöksyy eteenpäin, tekee vihollisille {s:damage} vahinkoa ja tainnuttaa ne {s:stun_duration} sekunniksi.<br> Lisää Caustic Finalea kaikkiin osuttuihin vihollisiin."
		"dac_ability_sandking_burrowstrike_Lore"								"Tunneliverkoston rakentaminen oli iso urakka. Sand King ei saa tuhota sitä. – <i>Jull, viinaparoni</i>"

		"dac_ability_sandking_caustic_finale"									"Caustic Finale"
		"dac_ability_sandking_caustic_finale_Description"						"PASSIIVINEN: Joka kerta, kun Sand King osuu viholliseen, hän vähentää kohteensa hyökkäysnopeutta {s:attack_slow} {s:slow_duration} sekunniksi. Jos kohde kuolee Caustic Finalen ollessa aktiivisena, se räjähtää sekä jakaa 1 ruudun päässä oleville liittolaisille {s:damage_expired} vahinkoa."
		"dac_ability_sandking_caustic_finale_Lore"								"Ei kai kaveri suojapeitteen käyttöön kuole? – <i>Anessix, koskematon infernalisti</i>"

		//SHADOW FRIEND
		"dac_ability_shadow_fiend_requiem"										"Requiem of Souls"
		"dac_ability_shadow_fiend_requiem_Description"							"Shadow Fiend kanavoi kirottujen sielut, vapauttaa ympärilleen demonienergiaa ja jakaa {s:damage} tuhoa kaikille, jotka osuvat energiapamauksen alueelle."
		"dac_ability_shadow_fiend_requiem_Lore"									"Minulla on Ristulin hovin kanssa diili. – <i>Anessix, maallinen demoni</i>"

		//PHANTOM ASSASSIN
		"dac_ability_phantom_assassin_coup_de_grace"							"Coup de Grace"
		"dac_ability_phantom_assassin_coup_de_grace_Description"				"PASSIIVINEN: Phantom Assassinilla on {s:crit_chance} % mahdollisuus jakaa vihollisille {s:crit_bonus} % kriittistä osumaa."
		"dac_ability_phantom_assassin_coup_de_grace_Lore"						"Hän tappoi Kwerthiasin. Ihan hyvin hän voi jahdata nyt minua. – <i>O'nia, vainoharhainen kauppias</i>"

		//DROW RANGER
		"dac_ability_drow_ranger_trueshot_aura"									"Precision Aura"
		"dac_ability_drow_ranger_trueshot_aura_Description"						"PASSIIVINEN: Drow Ranger lisää {s:bonus} % hyökkäysnopeutta kaikille {s:range} ruudun päässä oleville liittolaisille."
		"dac_ability_drow_ranger_trueshot_aura_Lore"							"Me pääsemme vielä huipulle, Traxex. Usko pois. – <i>Grek, huijari</i>"

		//MORPHLING
		"dac_ability_morphling_waveform"										"Waveform"
		"dac_ability_morphling_waveform_Description"							"Morphling sulaa nesteeksi, syöksyy eteenpäin ja aiheuttaa tielleen osuville vihollisille {s:damage} vahinkoa.\n\nMorphling on vahingoittumaton Waveformin aikana."
		"dac_ability_morphling_waveform_Lore"									"Huolestuttavaa, että Turath mielistelee Morphlingin kaltaisia palvelusten toivossa. – <i>Quiet, juonittelijarobotti</i>"

		//AXE
		"dac_ability_axe_berserkers_call"										"Berserker's Call"
		"dac_ability_axe_berserkers_call_Description"							"Axe pilkkaa vihollisia {s:radius} ruudun sisällä ja pakottaa ne hyökkäämään kimppuunsa. Berserker's Callin vaikutuksen aikana Axe saa {s:bonus_armor} lisäpanssaria."
		"dac_ability_axe_berserkers_call_Lore"									"Axe on aina valmis auttamaan. Riittää, kun se saa olutta ja luvan kertoa hyvän tarinan. – <i>Wren, kiihottajakapinoija</i>" 

		//RAZOR
		"dac_ability_razor_plasma_field"										"Plasma Field"
		"dac_ability_razor_plasma_field_Description"							"Razor purkaa salamakehän, joka voimistuu kasvaessaan ja iskee kipinää kutistuessaan. Se hidastuu ja aiheuttaa jopa {s:damage_max} vahinkoa vihollisille {s:radius} ruudun päässä.<br><br>Vahinko ja hidastus kasvavat sitä mukaa, mitä kauempana Razor on.<br><br> Plasma-aalto osuu jokaiseen kahdesti: kerran kasvaessaan ja toisen kutistuessaan."
		"dac_ability_razor_plasma_field_Lore"									"Jos hän on sinusta pelottava White Spiren kaduilla, näkisitpä hänet Narrow Mazessa. – <i>Anessix, penkkiurheilija</i>"

		//CRYSTAL MAIDEN
		"dac_ability_crystal_maiden_brilliance_aura"							"Arcane Aura"
		"dac_ability_crystal_maiden_brilliance_aura_Description"				"PASSIVIINEN: Joka {s:interval} sekunnin välein Crystal Maiden antaa kaikille liittolaisille {s:mana_regen} mananpalautumista."
		"dac_ability_crystal_maiden_brilliance_aura_Lore"						"Ota selvää, jahtaako hän minua vai Turathia. – <i>Jull, Jääraunioiden salakauppias</i>"

		//KUNKKA
		"dac_ability_kunkka_ghostship"											"Ghostship"
		"dac_ability_kunkka_ghostship_Description"								"Kunkka kutsuu esiin aavelaivan, joka purjehtii halki taistelutantereen, ennen kuin hajoaa kappaleiksi. Se aiheuttaa {s:ability_damage} vahinkoa ja tainnuttaa kaikki viholliset {s:ghostship_crash_cells} ruudun päässä hylystä {s:stun_duration} sekunniksi."
		"dac_ability_kunkka_ghostship_Lore"										"Eeb-mammalla oli paljon kavereita. Kunkka janoaa vastauksia yhtä paljon kuin minä. – <i>Wren, kostonhimoinen tytär</i>"

		//TINY
		"dac_ability_tiny_toss"													"Toss"
		"dac_ability_tiny_toss_Description"										"Tiny nappaa sattumanvaraisesti lähellä olevan vihollisen ja nakkaa sen kauimpana olevaa viholliskohdetta kohti. Törmäyksessä kohde aiheuttaa {s:damage} vahinkoa kaikille vihollisille {s:damage_radius} ruudun säteellä ja tainnuttaa ne {s:stun_duration} sekunniksi."
		"dac_ability_tiny_toss_Lore"											"Ennolla oli näköjään lihaskimppu apunaan. – <i>Grekillä on isommanpuoleinen ongelma</i>"

		//PUCK
		"dac_ability_puck_illusory_orb"											"Illusory Orb"
		"dac_ability_puck_illusory_orb_Description"								"Puck laukaisee suorassa linjassa leijailevan taikapallon, joka aiheuttaa sen tielle osuville vihollisille {s:damage} vahinkoa."
		"dac_ability_puck_illusory_orb_Lore"									"Moni kakku päältä kaunis. Puck on oikeasti roisto. – <i>Hobgen latelee elämänviisauksia</i>"

		"dac_ability_puck_phase_shift"											"Phase Shift"
		"dac_ability_puck_phase_shift_Description"								"<i>LOHIKÄÄRMEIDEN LIITTO AVAAMA</i><br>PASSIIVINEN: Juuri ennen vahingon ottamista Puck vaihtaa toiseen ulottuvuuteen, jossa se on immuuni tämän maailman hyökkäyksille {s:shift_duration} sekunnin ajan."
		"dac_ability_puck_phase_shift_Lore"										"That little bugger is never around when you need him. - <i>Hobgen, Impatient Underlord</i>"

		//CLOCKWERK
		"dac_ability_clockwerk_battery_assault"									"Battery Assault"
		"dac_ability_clockwerk_battery_assault_Description"						"Clockwerk laukaisee voimakkaita sirpaleita sattumanvaraisia vihollisia kohti {s:radius_in_cells} ruudun säteellä {s:duration} sekunniksi sekä aiheuttaa {s:damage} maagista vahinkoa ja minitainnutuksen."
		"dac_ability_clockwerk_battery_assault_Lore"							"Brass Herald kutsuisi sinua iljetykseksi, mutta minusta sinä olet varsin fiksu pikku-Keen. – <i>Quiet, harhaoppinen automaatti</i>"

		//LICH
		"dac_ability_lich_chain_frost"											"Chain Frost"
		"dac_ability_lich_chain_frost_Description"								"Lich vapauttaa jääpallon, joka kimpoaa {s:jumps} kertaa lähellä olevien vihollisten välillä. Se hidastaa niiden hyökkäys- ja liikenopeutta {s:slow_value} % {s:slow_duration} sekunniksi ja aiheuttaa {s:damage} vahinkoa jokaisella osumalla."
		"dac_ability_lich_chain_frost_Lore"										"Voimme auttaa toisiamme, Lich. –<i>Turath tekee sopimuksen paholaisen kanssa</i>"

		//TIDEHUNTER
		"dac_ability_tidehunter_ravage"											"Ravage"
		"dac_ability_tidehunter_ravage_Description"								"Tidehunter läimäyttää maata ja lonkerot purkautuvat joka suuntaan. Tämä aiheuttaa {s:damage} vahinkoa ja lamaannuttaa kaikki viholliset {s:cells} ruudun säteellä {s:duration} sekunniksi."
		"dac_ability_tidehunter_ravage_Lore"									"Kunkka, olit parempaa seuraa, kun sinulla ei ollut leviathania kintereillä. –<i>Wren, pinnallinen ystävä</i>"

		//SHADOWSHAMAN
		"dac_ability_shadow_shaman_voodoo"										"Hex"
		"dac_ability_shadow_shaman_voodoo_Description"							"Shadow Shaman noituu vihollisen harmittomaksi olennoksi, joka ei voi hyökätä tai käyttää kykyjään {s:duration} sekunnin ajan."
		"dac_ability_shadow_shaman_voodoo_Lore"									"On siinä varsinainen showmies! – <i>Grek kehuu työkaveria</i>"

		//ENIGMA
		"dac_ability_enigma_midnight_pulse"										"Midnight Pulse"
		"dac_ability_enigma_midnight_pulse_Description"							"Enigma lähettää {s:radius} ruudun päähän tumman värähtelyn, joka aiheuttaa vihollisille vahinkoa {s:damage_percent} % niiden enimmäisterveydestä {s:duration} sekunniksi."
		"dac_ability_enigma_midnight_pulse_Lore"								"Kaikkea ei voi maailmassa hallita. – <i>Turath, hullu velho</i>"

		//TINKER
		"dac_ability_tinker_heat_seeking_missile"								"Heat-Seeking Missile"
		"dac_ability_tinker_heat_seeking_missile_Description"					"Tinker ampuu ohjuksilla {s:targets} satunnaista vihollista ja aiheuttaa osuessaan {s:damage_per_missile} vahinkoa."
		"dac_ability_tinker_heat_seeking_missile_Lore"							"Samanmielisten keenien yhteisö tietää ongelmia, jos sitä ei pidetä silmällä. – <i>O'nia yrittää yhdistää joukot </i>"

		//SNIPER
		"dac_ability_sniper_assassinate"										"Lahtarit"
		"dac_ability_sniper_assassinate_Description"							"Sniper tähtää vihollista {s:castPoint} sekunnin ajan ja ampuu tuhoisan laukauksen, joka aiheuttaa {s:damage} vahinkoa ja minitainnuttaa kohteen."
		"dac_ability_sniper_assassinate_Lore"									"Minulla oli ennen lämmittelijänä keeni. Yleisö tykkäsi, vakuutusyhtiö ei niinkään. – <i>Grek keskittyy olennaiseen</i>"

		//NECROLYTE
		"dac_ability_necrophos_death_pulse"										"Death Pulse"
		"dac_ability_necrophos_death_pulse_Description"							"Necrophos laukaisee ympärilleen kuolonaallon, joka vaikuttaa vihollisiin ja liittolaisiin {s:radius} ruudun päähän.<br><br> Isku aiheuttaa {s:damage} vahinkoa vihollisille ja parantaa liittolaisia {s:heal}."
		"dac_ability_necrophos_death_pulse_Lore"								"Kakista jo Necrophos. Kyllä sinäkin jotain halajat. – <i>Anessix, lipeväkielinen paholainen</i>"

		//SLARDAR
		"dac_ability_slardar_amplify_damage"									"Corrosive Haze"
		"dac_ability_slardar_amplify_damage_Description"						"Slardar modifies enemy armor by {s:armor_reduction} for {s:duration} seconds."
		"dac_ability_slardar_amplify_damage_Lore"								"Yhteistyö sujui Slarkin kanssa hyvin, joten kyllä mekin sopimukseen pääsemme. – <i>Jull, kiljufani</i>"

		//BEASTMASTER
		"dac_ability_beastmaster_wild_axes"										"Wild Axes"
		"dac_ability_beastmaster_wild_axes_Description"							"Beastmaster sinkoaa kirveet ilmoille ja kutsuu ne takaisin. Ne silpovat vihollisia ja jakavat {s:axe_damage} vahinkoa.\n\nJokainen kirves voi osua viholliseen vain kerran ja vahvistaa Beastmasterin tekemän vahingon määrää {s:damage_amp} prosentilla."
		"dac_ability_beastmaster_wild_axes_Lore"								"Moniko puunhalaaja asettuu rääpäleen puolelle? – <i>Grek valittaa Ennosta</i>"

		//VENOMANCER
		"dac_ability_venomancer_plague_ward"									"Plague Ward"
		"dac_ability_venomancer_plague_ward_Description"						"Venomancer kutsuu {s:num_wards} Plague Wardia taistelemaan rinnallaan."
		"dac_ability_venomancer_plague_ward_Lore"								"Luomuksesi ovat kauniita. Autan sinua puutarhanhoidossa... – <i>Turath tajuaa, että mielistelyllä pääsee pitkälle</i>"

		//TEMPLAR ASSASSIN
		"dac_ability_templar_assassin_refraction"								"Refraction"
		"dac_ability_templar_assassin_refraction_Description"					"Templar Assassin hämärtää olemuksensa välttääkseen vahinkoa ja saadakseen {s:bonus_damage} % lisävahinkoa.\n\nVahinko ja sen välttäminen ovat erillisiä, ja niitä on {s:instances} kutakin."
		"dac_ability_templar_assassin_refraction_Lore"							"Jos autat minua, olemme sujut. – <i>Wren pyytää apua kaikkialta</i>"
		
		//VIPER
		"dac_ability_viper_viper_strike"										"Viper Strike"
		"dac_ability_viper_viper_strike_Description"							"Viperin hapan myrkky hidastaa vihollisyksikön liikettä ja hyökkäysnopeutta {s:bonus_attack_speed} sekä jakaa samalla {s:damage} myrkkyvahinkoa ajan mittaan.<br><br>Hidastus heikkenee myrkyn vaikutuksen aikana."
		"dac_ability_viper_viper_strike_Lore"									"Olemme lopulta hyvin samanlaisia. Minäkin tiedän, mitä orjuus on. – <i>Quiet, juonitteleva robotti</i>"

		"dac_ability_viper_corrosive_skin"										"Corrosive Skin"
		"dac_ability_viper_corrosive_skin_Description"							"<i>LOHIKÄÄRMEIDEN LIITON AVAAMA</i><br>PASSIIVINEN: Viper muodostaa tarttuvan myrkyn, jakaa {s:damage_per_second} vahinkoa sekuntia kohti sekä hidastaa vihollisen hyökkäysnopeutta aiheuttaen vahinkoa {s:dot_duration} sekunnin ajan. Hapon erittyminen kasvattaa Viperin loitsunvastustusta +{s:magic_resistance}."
		"dac_ability_viper_corrosive_skin_Lore"									"I'd love to figure out a way to make a coat out of that one... - <i>Carl, Big Dreams</i>"

		//LUNA
		"dac_ability_luna_moon_glaive"											"Moon Glaives"
		"dac_ability_luna_moon_glaive_Description"								"PASSIIVINEN: Lunan heittoterät kimpoilevat vihollisesta toiseen {s:range} ruudun säteellä.<br><br>Jokainen pomppu vähentää vahinkoa {s:damage_reduction_percent} prosenttia."
		"dac_ability_luna_moon_glaive_Lore"										"En tajua puoliakaan puheestasi, mutta hienosti sinä heität. – <i>Grek, laumaeläin</i>"

		//DRAGON KNIGHT
		"dac_ability_dragon_knight_breathe_fire"								"Breathe Fire"
		"dac_ability_dragon_knight_breathe_fire_Description"					"Dragon Knight syöksee tulta edessä olevien vihollisten päälle, jakaa {s:damage} vahinkoa sekä vähentää niiden hyökkäysvahinkoa {s:attack_reduction} {s:duration} sekunniksi.
"
		"dac_ability_dragon_knight_breathe_fire_Lore"							"Davion, your talents are being wasted on someone who doesn't appreciate the beauty of what you do. - <i>In which Hobgen tries to poach talent</i>"

		"dac_ability_dragon_knight_elder_dragon_form"							"Elder Dragon Form"
		"dac_ability_dragon_knight_elder_dragon_form_Description"				"<i>LOHIKÄÄRMEIDEN LIITON AVAAMA</i><br>PASSIIVINEN: Dragon Knight ottaa taistelun aluksi yhden kolmesta vanhojen ja voimakkaiden lohikäärmeiden muodoista, mikä nostaa hänen nopeuttaan {s:bonus_movement_speed} % ja hyökkäysvahinkoa {s:bonus_attack_damage} ja antaa sille uusia voimia.<br><br>Vihreä lohikäärme:\nmyrkyttävät hyökkäykset tekevät {s:corrosive_breath_damage} vahinkoa sekuntia kohti {s:corrosive_breath_duration} sekunnin ajan.<br><br>Punainen lohikäärme:\nvihollisten myrkytyksen lisäksi vahingoittaa kaikkia vihollisia {s:splash_radius} ruudun päässä kohteesta ja jakaa {s:splash_damage_percent} % vahinkoa.<br><br>Sininen lohikäärme:\n vihollisten myrkytyksen lisäksi hidastaa {s:frost_aoe} ruudun päässä olevien vihollisten liikenopeutta {s:frost_bonus_movement_speed} % ja hyökkäysnopeutta {s:frost_bonus_attack_speed} % {s:frost_duration} sekunniksi."
		"dac_ability_dragon_knight_elder_dragon_form_Lore"						"Kaden kehui sinua. Toivottavasti aiheesta. – <i>Wren, lohikäärmeiden ystävä</i>"

		//ENCHANTRESS
		"dac_ability_enchantress_natures_attendants"							"Nature's Attendants"
		"dac_ability_enchantress_natures_attendants_Description"				"Enchantress kutsuu {s:wisp_count} virvatulta, joka parantaa liittolaiset {s:radius} ruudun säteellä.<br><br>Liittolaiset saavat {s:heal} terveyttä {s:heal_interval} sekunnin välein ja {s:duration} sekunnin ajan."
		"dac_ability_enchantress_natures_attendants_Lore"						"Hän ei selviä päivääkään White Spiressä. Ei päivääkään. – <i>Hobgen aliarvioi vastustajansa</i>"

		//BOUNTY HUNTER
		"dac_ability_bounty_hunter_shuriken_toss"								"Shuriken Toss"
		"dac_ability_bounty_hunter_shuriken_toss_Description"					"Bounty Hunter heittää vihollista tappavalla heittotähdellä, joka aiheuttaa {s:damage} vahinkoa ja minitainnuttaa vihollisen."
		"dac_ability_bounty_hunter_shuriken_toss_Lore"							"Luota siihen, jonka lojaalius on myytävänä, niin kauan kuin rahaa riittää. – <i>O'nia ja kyseenalainen viisaus</i>"

		//BATRIDER
		"dac_ability_batrider_sticky_napalm"									"Sticky Napalm"
		"dac_ability_batrider_sticky_napalm_Description"						"Batrider heittää maahan pullon ja peittää maan tahmealla öljyllä {s:radius} ruudun päässä osumakohdasta. Öljy lisää Batriderin iskujen {s:damage} vahinkoa loitsua kohti sekä hidastaa vihollisten liike- ja hyökkäysnopeutta {s:movement_speed_pct} %.<br><br>Sticky Napalmin seuraavat käyttökerrat lisäävät vahinkoa ja laskevat prosentteja 10 loitsuun."
		"dac_ability_batrider_sticky_napalm_Lore"								"Mahtava tyyppi. Kerrassaan mahtava. –<i>Hobgen, pyromaanikko</i>"

		//DOOM BRINGER
		"dac_ability_doom_bringer_doom"											"Doom"
		"dac_ability_doom_bringer_doom_Description"								"Doom langettaa kirouksen, joka estää vihollista käyttämästä loitsuja tai esineitä. Samalla ne ottavat {s:damage} vahinkoa sekunnissa.\n\nKestää {s:duration} sekuntia."
		"dac_ability_doom_bringer_doom_Lore"									"Mieluiten teen sopimuksia vertauskuvallisten demonien kanssa. – <i>O'nia ottaa kantaa Anessixin liittolaisiin</i>"

		//GYROCOPTER
		"dac_ability_gyrocopter_call_down"										"Call Down"
		"dac_ability_gyrocopter_call_down_Description"							"Gyrocopter tekee {s:radius} ruudun päässä ohjusiskun.<br><br>Kaksi ohjusta tulee peräkanaa. Ensimmäinen tekee {s:damage_first} vahinkoa ja hidastaa {s:slow_first} % {s:slow_duration_first} sekunniksi. Toinen aiheuttaa {s:damage_second} vahinkoa ja hidastaa {s:slow_second} % {s:slow_duration_second} sekunniksi."
		"dac_ability_gyrocopter_call_down_Lore"									"Mieti, miten tavara liikkuisi, jos hän tulisi apuun. – <i>Jull suunnittelee pitkällä tähtäimellä</i>"

		//ALCHEMIST
		"dac_ability_alchemist_acid_spray"										"Acid Spray"
		"dac_ability_alchemist_acid_spray_Description"							"Alchemist ruiskuttaa korkeapaineista happoa {s:radius} ruudun päähän.<br><br>Saastuneella alueella kävelevät viholliset ottavat {s:damage} vahinkoa sekunnissa, ja niiden panssari heikentyy {s:armor_reduction}."
		"dac_ability_alchemist_acid_spray_Lore"									"Siinä poltat ihmisten kasvoja, minä taloja. Ollaan täydellinen pari. – <i>Hobgen kuroo umpeen kultturierojen kuilun</i>"

		// Lycan
		"dac_ability_lycan_wolf_spawn_shift"									"Summon Wolves"
		"dac_ability_lycan_wolf_spawn_shift_Description"						"Lycan kutsuu kaksi sutta taistelemaan rinnallaan.<br><br>Sitten Lycan muuntautuu ja saa {s:hp_per} % lisää enimmäisterveyteen. Sen liikenopeus kasvaa {s:speed} ja saa {s:crit_chance} % prosentin mahdollisuuden kriittiseen osumaan tehdäkseen {s:crit_bonus} % lisävahinkoa."
		"dac_ability_lycan_wolf_spawn_shift_Lore"								"Auta minua kostamaan Lycan. – <i>Wren vetoaa Lycanin inhimillisyyteen</i>"
		
		// LONE DRUID
		"dac_ability_lone_druid_summon_bear"									"Summon Spirit Bear"
		"dac_ability_lone_druid_summon_bear_Description"						"Kutsuu voimakkaan karhukumppanin."
		"dac_ability_lone_druid_summon_bear_Lore"								"Pidä karhu kaukana. En kestä karhuja. – <i>Grek, karhupelkoinen</i>"
		

		//CHAOS KNIGHT
		"dac_ability_chaos_knight_chaos_bolt"									"Chaos Bolt"
		"dac_ability_chaos_knight_chaos_bolt_Description"						"Chaos Knight sinkoaa ilmoille mystisen energiasalaman, joka tainnuttaa vihollisen epämääräiseksi ajaksi ja tekee satunnaista vahinkoa."
		"dac_ability_chaos_knight_chaos_bolt_Lore"								"Pikkukaaos tekisi White Spirelle terää. – <i>Hobgen, kaaoksen lähettiläs</i>"

		//OGRE MAGI
		"dac_ability_ogre_magi_bloodlust"										"Bloodlust"
		"dac_ability_ogre_magi_bloodlust_Description"							"Ogre Magi yllyttää liittolaisen kiihkoon siten, että sen liikenopeus kasvaa {s:bonus_movement_speed} % ja hyökkäysnopeus lisääntyy {s:bonus_attack_speed} %. Jos loitsua käytetään Ogre Magiin, hyökkäysnopeus on sen sijaan {s:self_bonus} %."
		"dac_ability_ogre_magi_bloodlust_Lore"									"Velho on kaksi kertaa hintansa väärti. Onneksi se on myös idiootti. – <i>O'nia, tarjousten metsästäjä</i>"

		//TREANT PROTECTOR
		"dac_ability_treant_leech_seed"											"Leech Seed"
		"dac_ability_treant_leech_seed_Description"								"Treant Protector plants a life-sapping seed in an enemy unit, draining its health by {s:leech_damage} every {s:damage_interval} seconds, while simultaneously slowing its attack and movement speed by {s:movement_slow}%.<br><br>The seed heals friendly units up to {s:radius} cell away from the target. Lasts {s:duration} seconds. Ends early if the target dies."
		"dac_ability_treant_leech_seed_Lore"									"Pakko myöntää. En olisi uskonut Ennon pyytävän puuta apuun.– <i>Jull ja Anessix käyvät paukulla</i>"

		//KEEPER OF THE LIGHT
		"dac_ability_keeper_of_the_light_illuminate"							"Illuminate"
		"dac_ability_keeper_of_the_light_illuminate_Description"				"Keeper of Light kanavoi energiaa, jonka voima kasvaa kanavoinnin edetessä. Kun energia vapautetaan, se hyökyy eteenpäin aaltona, joka aiheuttaa {s:damage_per_second} vahinkoa sekunnissa."
		"dac_ability_keeper_of_the_light_illuminate_Lore"						"Keeper on mahtavampi, kuin antaa olettaa. Tästä seuraa tuhoa, jos sitä ei panna aisoihin. – <i>Turath, valon sammuttaja</i> "

		// MEDUSA
		"dac_ability_medusa_stone_gaze"											"Stone Gaze"
		"dac_ability_medusa_stone_gaze_Description"								"Vihollisen liike- ja hyökkäysnopeus hidastuu {s:slow} %, kun se katsoo Medusaa {s:radius} ruudun päästä.<br><br>Jos vihollinen katsoo Medusaa yhteensä {s:face_duration} sekuntia, se kivettyy.<br><br>Kivettyneet viholliset ovat tainnoksissa ja ottavat {s:bonus_physical_damage} fyysistä lisävahinkoa."
		"dac_ability_medusa_stone_gaze_Lore"									"Minun tyyppiäni, mutta minä haluan Mireskan. – <i>Hobgen, rakastunut roisto</i>"

		"dac_ability_medusa_split_shot"											"Split Shot"
		"dac_ability_medusa_split_shot_Description"								"PASSIVE: Medusa magically shoots up to {s:extra_shots} extra arrows, hitting additional targets within {s:range} cell of her primary target. The additional arrows deal {s:damage_modifier} percent of her primary arrow's damage."
		"dac_ability_medusa_split_shot_Lore"									"Oletko nähnyt valtiattaren ampuvan jousipyssyllä? Sinulla näyttää henki vielä kulkevan, joten et liene ole... <i>Grek puhuu viimeisimmästä rekrytoinnistaan</i>"

		// TROLL WARLORD
		"dac_ability_troll_warlord_fervor"										"Fervor"
		"dac_ability_troll_warlord_fervor_Description"							"PASSIIVINEN: Troll Warlordin sateet puhaltavat iskun jälkeen ja saa {s:attack_speed} % lisää hyökkäysnopeutta.<br><br>Jos peikko vaihtaa kohdettaan, se menettää hyökkäysnopeutensa.<br><br>Efektit voi laskea yhteen enintään {s:max_stacks} kertaa."
		"dac_ability_troll_warlord_fervor_Lore"									"Aikaansaava ja armoton, kuten minä. – <i>Quiet, ei turhaa huhkintaa</i>"

		// SHREDDER
		"dac_ability_timbersaw_whirling_death"									"Whirling Death"
		"dac_ability_timbersaw_whirling_death_Description"						"Timbersaw'n panssarin terävät kärjet pyörivät ja aiheuttavat vihollisille {s:damage} vahinkoa {s:radius} ruudun säteellä."
		"dac_ability_timbersaw_whirling_death_Lore"								"Aavistin, että Enno kutsuu apuun metsäkamunsa, joten minäkin pyysin apua. – <i>Hobgen ryhtyy ratkomaan ongelmia</i>"

		//TUSK
		"dac_ability_tusk_walrus_punch"											"Walrus PUNCH!"
		"dac_ability_tusk_walrus_punch_Description"								"Tusk täräyttää mahtavan mursulyönnin. Se on niin voimakas, että uhri lentää ilmaan. Uhri hidastuu {s:move_slow} %, kun putoaa maahan."
		"dac_ability_tusk_walrus_punch_Lore"									"Olimme kerran kavereita. Kohta nähdään, tuliko hän auttamaan vai lyömään. – <i>Jull muistelee nuoruuttaan</i>"
		
		
		//ABADDON
		"dac_ability_abaddon_aphotic_shield"									"Aphotic Shield"
		"dac_ability_abaddon_aphotic_shield_Description"						"Abaddon suojelee liittolaista kilvellä ja ottaa {s:damage_absorb} vahinkoa {s:duration} sekunnin ajan.<br><br>Kun kilpi tuhotaan, se räjähtää ja aiheuttaa {s:damage_absorb} vahinkoa vihollisille {s:radius} ruudun säteellä.<br><br>Tietyt heikennykset ja tainnutukset poistetaan."
		"dac_ability_abaddon_aphotic_shield_Lore"								"Arvostan kovasti Avernuksen sukua. – <i>O'nia mielistelee palveluksen toiveessa</i>"

		// TERRORBLADE
		"dac_ability_terrorblade_metamorph"										"Metamorphosis"
		"dac_ability_terrorblade_metamorph_Description"							"Terrorblade muuttuu vahvaksi demoniksi ja saa {s:bonus_attack_range} ruudun kaukohyökkäyksen ja {s:bonus_attack_damage} % lisävahinkoa ja -hyökkäysnopeutta. Terrorblade vaihtaa terveyttä liittolaisen kanssa kyvyn käynnistyessä."
		"dac_ability_terrorblade_metamorph_Lore"								"Kun tuo juttelee Anessixin kanssa, tiedossa on jotain pahaa. – <i>Jull, tarkkailija</i>"

		// TECHIES
		"dac_ability_techies_bomb" 												"Remote Mines"
		"dac_ability_techies_bomb_Description"									"Jengi asentaa pommin, joka räjähtää {s:explode_time} sekunnin päästä ja aiheuttaa {s:damage} vahinkoa {s:range} ruudun säteellä.<br><br>Aiheuttaa puolikkaan vahingon vihollisille, jotka ovat puolimatkassa."
		"dac_ability_techies_bomb_Lore"											"Sano mitä sanot, mutta keenit osaavat kyllä hauskuttaa. – <i>Hobgen seuraa romutusta</i>"

		//DISRUPTOR
		"dac_ability_disruptor_static_storm"									"Static Storm"
		"dac_ability_disruptor_static_storm_Description"						"Disruptor luo tuhoisan, staattisen myrskyn, joka hiljentää ja sokeuttaa ({s:miss_chance} %) kaikki viholliset {s:duration} sekunniksi enintään {s:radius} ruudun päässä keskustasta.<br><br>Vahinko alkaa heikkona mutta voimistuu, kunnes saavuttaa huippunsa: {s:damage_max}."
		"dac_ability_disruptor_static_storm_Lore"								"En tiedä, tuliko hän auttamaan vai vahtimaan Axea. – <i>Wren, sekalaisen sakin johtaja</i>"

		//OMNIKNIGHT
		"dac_ability_omniknight_purification"									"Purification"
		"dac_ability_omniknight_purification_Description"						"Omniknight parantaa liittolaisen välittömästi {s:heal} pisteellä ja vahingoittaa samalla pistemäärällä kaikkia vihollisia {s:radius} ruudun päässä."
		"dac_ability_omniknight_purification_Lore"								"Hän käyttää uskoa aseenaan, kuten minäkin. – <i>Turath, harhainen pelastaja</i>"
		
		//PUDGE
		"dac_ability_pudge_meathook"											"Meat Hook"
		"dac_ability_pudge_meathook_Description"								"Pudge linkoaa verisen koukun kohti kauinta vihollista, vetää sen takaisin ja aiheuttaa {s:damage} vahinkoa.<br><br>Pudge ja liittolaiset yhden ruudun säteellä keskittävät iskunsa uhriin, kunnes se kuolee."
		"dac_ability_pudge_meathook_Lore"										"Pudge on mahtava. Tervehtii aina, kun parkitsen ruumiita. – <i>Carl, kuolematon vaatturi</i>"
		
		//WARLOCK
		"dac_ability_warlock_shadow_word"										"Shadow Word"
		"dac_ability_warlock_shadow_word_Description"							"Warlock kuiskaa loitsun, joka antaa liittolaiselle {s:heal_tick} terveyttä tai aiheuttaa viholliselle {s:damage_tick} vahinkoa joka sekunti {s:duration} sekunnin ajan."
		"dac_ability_warlock_shadow_word_Lore"									"Joitain salaisuuksia ei paljasteta. Demnok saa vielä maksaa lörpöttelystään. – <i>Anessix odottaa tilaisuuttaan</i>"
		
		//BLOODSEEKER
		"dac_ability_bloodseeker_blood_rage"									"Bloodrage"
		"dac_ability_bloodseeker_blood_rage_Description"						"PASSIIVINEN: Bloodseekerin joka sekunti kärsimä vahinko vastaa {s:self_dmg_pct} % sen enimmäisterveydestä.<br><br>Jokaisesta puuttuvasta terveydestä se saa {s:attack_speed_per_missing_pct} % hyökkäysnopeutta.<br><br>Taposta hän saa takaisin {s:heal_pct} % sen enimmäisterveydestä."
		"dac_ability_bloodseeker_blood_rage_Lore"								"Jos minä jotain olen urallani oppinut, kansa janoaa verta. Palkkaa hänet. – <i>Grek, päättäväinen promoottori</i>"
		
		//ARC WARDEN
		"dac_ability_arc_warden_tempest_double"									"Tempest Double"
		"dac_ability_arc_warden_tempest_double_Description"						"Arc Warden luo kopion itsestään.<br><br>Kopio voi käyttää kaikkia Arc Wardenin esineitä ja loitsuja, mutta sillä on eri latausajat. Arc Warden -kopio luo {s:mana_dampen_outgoing} % manaa tehdyistä hyökkäyksistä muttei ottamastaan vahingosta."
		"dac_ability_arc_warden_tempest_double_Lore"							"Hän tuo mukanaan luomisen ihmeet. – <i>Turath, kryptinen profeetta</i>"
		
		//MIRANA
		"dac_ability_mirana_arrow"												"Sacred Arrow"
		"dac_ability_mirana_arrow_Description"									"Mirana ampuu tappavalla tarkkuudella pitkän kantaman nuolen, joka tainnuttaa kohteen {s:duration} sekunniksi ja aiheuttaa {s:damage} vahinkoa."
		"dac_ability_mirana_arrow_Lore"											"Kiihkoilijoihin ei kannata ikinä luottaa Wren. – <i>Jull jakelee ystävällisiä neuvoja</i>"
		
		//SLARK
		"dac_ability_slark_essence_shift"										"Essence Shift"
		"dac_ability_slark_essence_shift_description"							"PASSIIVINEN: Slarkin jokainen hyökkäys alentaa kohteen vahinkoa {s:stolen_dmg} ja hyökkäysnopeutta {s:stolen_aspd}.<br><br>Slarkin vahinko ja hyökkäysnopeus kasvavat samassa määrin, kuin mitä kohteelta on viety."
		"dac_ability_slark_essence_shift_lore"									"Slarkilla on avainrooli Pimeän riutan tavaran toimittamisessa. – <i>Jull, oman elämänsä sankari</i>"

		"dac_ability_slark_pounce"												"Pounce"
		"dac_ability_slark_pounce_description"									"PASSIIVINEN: Slark laskeutuu hypystään ja riisuu kohteensa aseista {s:disarm_duration} sekunniksi."
		"dac_ability_slark_pounce_lore"											"Kuinka hän sai meidät jälleen estettyä? – <i>Hobgen, tulisielu</i>"

		//BLACK DRAGON
		"dac_ability_black_dragon_splash_attack"								"Splash Attack"
		"dac_ability_black_dragon_splash_attack_description"					"PASSIIVINEN: Black Dragonin kaukohyökkäys aiheuttaa aluevahinkoa yksiköille, jotka ovat yhden ruudun säteellä kohteesta."
		"dac_ability_black_dragon_splash_attack_lore"							" "

		//"SVEN GREAT CLEAVE - used for YEAR BEAST and ROSHAN"
		"dac_ability_sven_great_cleave"											"Great Cleave"
		"dac_ability_sven_great_cleave_description"								"PASSIIVINEN: Lähitaisteluhyökkäykset osuvat lähellä oleviin vihollisiin."
		"dac_ability_sven_great_cleave_lore"									" "

		//"ROSHAN SPELL IMMUNE"
		"dac_ability_roshan_spell_immune"										"Spell Immune"
		"dac_ability_roshan_spell_immune_description"							"ACTIVE: Roshan becomes spell immune for {s:duration} seconds."
		"dac_ability_roshan_spell_immune_lore"									" "

		// Lone Druid Bear
		"dac_ability_lone_druid_spirit_bear_entangle"							"Entangling Claws"
		"dac_ability_lone_druid_spirit_bear_entangle_description"				"Spirit Bearin hyökkäyksillä voi kietoa ja tainnuttaa kohteen."
		"dac_ability_lone_druid_spirit_bear_entangle_lore"						" "
	}
}